// function dom sv info
function getSVInfo() {
    var ma = document.getElementById("txtMaSV").value;
    var ten = document.getElementById("txtTenSV").value;
    var email = document.getElementById("txtEmail").value;
    var password = document.getElementById("txtPass").value;
    var toan = document.getElementById("txtDiemToan").value*1;
    var ly = document.getElementById("txtDiemLy").value*1;
    var hoa = document.getElementById("txtDiemHoa").value*1;
    return new SinhVien(ma,ten,email,password,toan,ly,hoa);
}
// function show thông tin lên info
function renderDSSV(DSSV) {
    var contentHTML = "";
    for(var i = 0; i<DSSV.length; i++)
    {
        var sv = DSSV[i];
        var content = `<tr>
        <td>${sv.ma}</td>
        <td>${sv.ten}</td>
        <td>${sv.email}</td>
        <td>${sv.tinhDTB()}</td>
        <td>
        <button class="btn btn-danger" onclick="deleteSV('${sv.ma}')"><i class="fa fa-trash-alt"></i></button>
        <button class="btn btn-warning" onclick="editSV('${sv.ma}')"><i class="fa fa-edit"></i></button>
        </td>
        </tr>`;
        contentHTML += content;
    }
    document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}

// function post JSON data
function postJSON (DSSV) {
    var dataJson = JSON.stringify(DSSV);
    localStorage.setItem("DSSV", dataJson);
}
