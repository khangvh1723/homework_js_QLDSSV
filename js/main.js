var DSSV = [];
// get data from localStorage
var dataJsonGet = localStorage.getItem("dssv");
console.log(dataJsonGet);
if(dataJsonGet!=null){
    DSSV = JSON.parse(dataJsonGet).map(item => {
        return new SinhVien(item.ma, item.ten, item.email, item.matKhau,item.toan,item.ly,item.hoa);
    });
    renderDSSV(DSSV);
}
function createSV() {
    // dom thông tin sv
    var sv = getSVInfo();
    // save thông tin sv vào object
    DSSV.push(sv);
    // dom sv to interface
    renderDSSV(DSSV);
    // post data to localStorage
    var dataJson = JSON.stringify(DSSV);
    localStorage.setItem("dssv", dataJson);
}

function deleteSV(ma) {
    // input
    var index = DSSV.findIndex(item => {
        return item.ma == ma;
    });
    // process
    DSSV.splice(index,1);
    postJSON(DSSV);
    // output
    renderDSSV(DSSV);
}

function editSV(ma) {
    // input
    var index = DSSV.findIndex(item => {
        return item.ma == ma;
    });
    // output
    var sv = DSSV[index];
    document.getElementById("txtMaSV").value = sv.ma;
    document.getElementById("txtMaSV").readOnly = true;
    document.getElementById("txtTenSV").value = sv.ten;
    document.getElementById("txtEmail").value = sv.email;
    document.getElementById("txtPass").value = sv.matKhau;
    document.getElementById("txtDiemToan").value = sv.toan;
    document.getElementById("txtDiemLy").value = sv.ly;
    document.getElementById("txtDiemHoa").value = sv.hoa;
}

function updateSV() {
    // input
    var sv = getSVInfo();
    var index = DSSV.findIndex(item => {
        return item.ma == sv.ma;
    });
    // process
    DSSV[index] = sv;
    postJSON(DSSV);
    // output
    renderDSSV(DSSV);
}

function resetForm() {
    var empty = "";
    document.getElementById("txtMaSV").value = empty;
    document.getElementById("txtTenSV").value = empty;
    document.getElementById("txtEmail").value = empty;
    document.getElementById("txtPass").value = empty;
    document.getElementById("txtDiemToan").value = empty;
    document.getElementById("txtDiemLy").value = empty;
    document.getElementById("txtDiemHoa").value = empty;
}
